
Primeiro intalar o Geckodriver

É um driver utilizado para realização de testes de software e atualmente obrigatório para operar o Selenium Webdriver com o Firefox.
Agora que já dei uma breve explicação, vamos colocar a mão na massa:
O primeiro passo para darmos andamento nesse processo é baixar o arquivo no site: https://github.com/mozilla/geckodriver/releases
Selecione a versão compatível com o seu sistema, no meu caso utilizei a versão [geckodriver-v0.21.0-linux64.tar.gz] (que é a última).


Navegue até a pasta onde realizou o download do arquivo e descompacte-o com o seguinte comando, que após a execução deverá apresentar o arquivo executável do geckodriver:
$ sudo tar -xvf geckodriver-v0.21.0-linux64.tar.gz

4. Agora precisamos apenas movê-lo para a pasta “/usr/local/bin” e para isso vamos executar o comando abaixo:
$ sudo mv geckodriver /usr/local/bin/

5. Agora que já realizamos os passos acima, precisamos constatar que o arquivo está na pasta correta, navegue até ela através do comando:
$ cd /usr/local/bin
e depois, execute o comando abaixo para listarmos os arquivos dentro desta pasta:
$ ls -l